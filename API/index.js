const express = require('express');
const router = express.Router();
const DatarTamuQuery = require('../DB/query');
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

router.post('/postdatatamu', jsonParser, (req, res) => {
    const data = {
        nohp: req.body.nohp,
        nama: req.body.nama,
        kenalan: req.body.kenalan,
        jumlah_orang: req.body.jumlah_orang,
        kehadiran: req.body.kehadiran,
        pesan: req.body.pesan
    }
    console.log(data)
    DatarTamuQuery.insertDaftarTamu(data)
        .then(daftar => {
            res.json({
                status: '200',
                Message: 'Ok'
            })
        })
});

router.post('/postcomment', jsonParser, (req, res) => {
    const NowDate = new Date()
    const data = {
        nama: req.body.nama,
        comment: req.body.comment,
        timestamp: NowDate
    }
    DatarTamuQuery.insertDataComment(data)
        .then(daftar => {
            res.json({
                status: '200',
                Message: 'Ok'
            })
        })
});

router.get('/getcomment', (req, res) => {
    DatarTamuQuery.getAllComment()
        .then(comment => {
            res.json({
                status: '200',
                Message: comment
            })
        })
});

router.post('/testingpost', jsonParser, (req, res) => {
    res.json({
        status: '200',
        Message: 'Ok'
    });
});

router.get('/gettamu', (req, res) => {
    DatarTamuQuery.getDaftarTamu()
        .then(daftar => {
            res.json({
                status: '200',
                Message: daftar
            })
        })
});



module.exports = router;