const knex = require ('knex');
const config = require('./knexfile');
const environment = 'development';
const environmentConfig = config[environment];

const connection = knex (environmentConfig);


module.exports = {connection};