const knex = require('./connection');

module.exports = {
    getDaftarTamu(){
        return knex.connection('confirmed_tamu')
    },
    insertDaftarTamu: function (daftar_tamu) {
        return knex.connection('confirmed_tamu')
        .insert(daftar_tamu, 'nama').then(ids => {
            return ids[0];
        });
    },
    insertDataComment: function (comment) {
        return knex.connection('comment_tamu')
        .insert(comment, 'nama').then(ids => {
            return ids[0];
        });
    },
    getAllComment(){
        return knex.connection('comment_tamu').orderBy('timestamp', 'desc')
    }
}