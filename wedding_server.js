const http = require('http');
var express = require('express'),
    app = express(),
    port =  3004,
    path = require('path');
// const app = express();
// var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var confirmation = require('./API/index');
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();



const server = http.createServer(app);

// app.listen(port, () => console.log(`Listening on port ${port}`))

//allow cors
// Add headers
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');
  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);
  // Pass to next layer of middleware
  next();
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/confirmation', confirmation)


// const server = http.createServer((req, res) => {
//   res.statusCode = 200;
//   res.setHeader('Content-Type', 'text/plain');
//   res.end('Hello World');
// });

app.get('/APITESTING', (req, res) => {
  res.json({
      status: '200',
      Message: 'Ok'
  });
});
app.post('/testpost',jsonParser, (req, res) => {
  res.json({
      status: '200',
      Message: req.body
  });
});

server.listen(port, () => console.log(`Listening on port ${port}`));


module.exports = app;